package com.example.sample.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.example.sample.bean.NewsTable
import com.example.sample.repos.NewsRepo
import com.example.sample.repos.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class NewsVM : ViewModel() {
    private val newsRepo: NewsRepo = NewsRepo()
    val _news = MutableLiveData<Result<List<NewsTable>>>()
    val news = _news

    init {
        fetchNews()
    }

    fun fetchNews() {
        viewModelScope.launch {
            newsRepo.fetchNews().onEach {
                _news.value = it
            }.collect()
        }
    }

    fun getAllNews() = news
}