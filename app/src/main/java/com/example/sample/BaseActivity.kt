package com.example.sample

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding


abstract class BaseActivity<T : ViewDataBinding> : AppCompatActivity() {


    protected lateinit var binding: T
    abstract fun getLayoutId(): Int
    abstract fun initControl()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, getLayoutId())
        initControl()
    }


    protected fun checkConnectivity() {
        val manager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = manager.activeNetworkInfo

        if (null == activeNetwork) {
            val dialogBuilder = AlertDialog.Builder(this)
            dialogBuilder.setMessage("Make sure that WI-FI or mobile data is turned on, then try again")
                .setCancelable(false)
                .setPositiveButton("Retry") { _, _ ->
                    recreate()
                }
                .setNegativeButton("Cancel") { _, _ ->
                    finish()
                }

            val alert = dialogBuilder.create()
            alert.setTitle("No Internet Connection")
            alert.setIcon(R.mipmap.ic_launcher)
            alert.show()
        }
    }

    protected fun showMessageDialog(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }
}
