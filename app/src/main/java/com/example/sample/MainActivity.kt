package com.example.sample

import android.content.Intent
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import com.example.sample.adapter.NewsAdapter
import com.example.sample.adapter.VerticalPageTransformer
import com.example.sample.bean.NewsTable
import com.example.sample.databinding.ActivityDashboardBinding
import com.example.sample.repos.Result
import com.example.sample.viewmodels.NewsVM
import com.google.firebase.auth.FirebaseAuth
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : BaseActivity<ActivityDashboardBinding>() {

    private lateinit var vm: NewsVM
    private var adapter: NewsAdapter? = null

    private val newsList = ArrayList<NewsTable?>()

    override fun getLayoutId() = R.layout.activity_dashboard

    override fun initControl() {
        vm = ViewModelProvider.NewInstanceFactory().create(NewsVM::class.java)
        setupNavDrawer()
        setupViewPager()

        binding.mainLayout.swipeRefreshLayout.setOnRefreshListener()
        {
            vm.fetchNews()
        }
        observeVM()
    }

    private fun setupViewPager() {
        adapter = NewsAdapter(
            newsList
        ) { position ->
            val intent = Intent(this, WebLinkOpen::class.java)
            intent.putExtra("url", newsList[position])
            startActivityForResult(intent, 555)
        }
        binding.mainLayout.newsFeedVPager.setPageTransformer(VerticalPageTransformer())
        binding.mainLayout.newsFeedVPager.adapter = adapter
        binding.mainLayout.newsFeedVPager.currentItem = newsList.size
    }

    private fun setupNavDrawer() {
        binding.navigationView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navMarket -> {

                }
                R.id.navNews -> {

                }
                R.id.navRecommendation -> {

                }
                R.id.navLogout -> {
                    FirebaseAuth.getInstance().signOut()

                    val intent = Intent(this@MainActivity, PhoneNumberActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

                    startActivity(intent)

                }
                R.id.navRefer -> {

                }
                R.id.navTerm -> {

                }
            }
            true
        }

        val drawerToggle = ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        binding.drawerLayout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()
    }

    private fun observeVM() {
        vm.getAllNews().observe(this, { result ->
            when (result) {
                is Result.Loading -> binding.mainLayout.swipeRefreshLayout.isRefreshing = true
                is Result.Success -> {
                    if (result.data == null) {
                        newsList.clear()
                        binding.mainLayout.emptyView.visibility = View.VISIBLE
                        binding.mainLayout.swipeRefreshLayout.isRefreshing = false
                    }
                    result.data?.let { news ->
                        newsList.clear()
                        newsList.addAll(news)
                        binding.mainLayout.emptyView.visibility = View.GONE
                        binding.mainLayout.swipeRefreshLayout.isRefreshing = false
                    }
                    adapter?.notifyDataSetChanged()
                }
                is Result.Error -> {
                    binding.mainLayout.emptyView.visibility = View.VISIBLE
                    binding.mainLayout.swipeRefreshLayout.isRefreshing = false
                    showMessageDialog("Some error occurred")
                }
            }
        })
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

}
