package com.example.sample.repos

import java.lang.Exception

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
    EMPTY,
}

sealed class Result<out T>(
    val status: Status,
    val data: T?,
    val message: String?,
    val exception: Exception?
) {

    data class Success<out R>(val _data: R?) : Result<R>(
        status = Status.SUCCESS,
        data = _data,
        message = null,
        exception = null
    )

    data class Error(val excep: Exception) : Result<Nothing>(
        status = Status.ERROR,
        data = null,
        exception = excep,
        message = excep.message
    )

    data class Loading<out R>(val _data: R?, val isLoading: Boolean) : Result<R>(
        status = Status.LOADING,
        data = _data,
        message = null,
        exception = null
    )

    data class Empty<R>(val _data: R?) : Result<R>(
        status = Status.EMPTY,
        data = null,
        message = null,
        exception = null
    )


}