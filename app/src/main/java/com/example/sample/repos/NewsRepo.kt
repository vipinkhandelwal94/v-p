package com.example.sample.repos

import com.example.sample.bean.NewsTable
import com.google.firebase.database.FirebaseDatabase
import kotlinx.coroutines.ExperimentalCoroutinesApi

class NewsRepo constructor(
) : Repo(FirebaseDatabase.getInstance()) {

    companion object {
        const val NEWS_REFERENCE = "news"
    }

    @ExperimentalCoroutinesApi
    fun fetchNews() = fetchList<NewsTable>()
}