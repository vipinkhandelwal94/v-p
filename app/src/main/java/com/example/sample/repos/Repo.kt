package com.example.sample.repos

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.callbackFlow

abstract class Repo constructor(
    val firebaseDatabase: FirebaseDatabase
) {
    @ExperimentalCoroutinesApi
    inline fun <reified T> fetchList() = callbackFlow<Result<List<T>>> {

        val postListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                this@callbackFlow.sendBlocking(Result.Error(error.toException()))
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val items = dataSnapshot.children.map { ds ->
                    ds.getValue(T::class.java)
                }
                this@callbackFlow.sendBlocking(Result.Success(items.filterNotNull()))
            }
        }

        firebaseDatabase.getReference(NewsRepo.NEWS_REFERENCE)
            .addValueEventListener(postListener)

        awaitClose {
            firebaseDatabase.getReference(NewsRepo.NEWS_REFERENCE)
                .removeEventListener(postListener)
        }
    }
}