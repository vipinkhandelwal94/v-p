package com.example.sample.adapter

import android.app.Activity
import android.content.Intent
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.sample.WebLinkOpen
import com.example.sample.bean.NewsTable
import com.example.sample.databinding.NewsFeedItemBinding
import me.thanel.swipeactionview.SwipeActionView
import me.thanel.swipeactionview.SwipeGestureListener
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

class NewsAdapter(
    private val newsList: List<NewsTable?>,
    private val callPositionLinkOpen: (position: Int) -> Unit,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return NewsListHolder(
            NewsFeedItemBinding.inflate(
                (parent.context as Activity).layoutInflater,
                parent,
                false
            )
        )
    }


    override fun getItemViewType(position: Int): Int {
        return 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is NewsListHolder) {
            newsList[position]?.let { news ->
                holder.newsFeedItemBinding.newsDetail = news
                Glide.with(holder.newsFeedItemBinding.ivImageView.context).load(news.urlToImage)
                    .centerCrop()
                    .into(holder.newsFeedItemBinding.ivImageView)
                holder.newsFeedItemBinding.tvDate.text = ZonedDateTime.parse(news.publishedAt).format(
                    DateTimeFormatter.ISO_LOCAL_DATE_TIME)
            }
            holder.newsFeedItemBinding.swipeView.swipeGestureListener = object : SwipeGestureListener {
                override fun onSwipeLeftComplete(swipeActionView: SwipeActionView) {
                    super.onSwipeLeftComplete(swipeActionView)
                    val intent = Intent(
                        holder.newsFeedItemBinding.root.context,
                        WebLinkOpen::class.java
                    )
                    intent.putExtra(
                        "URL",
                        holder.newsFeedItemBinding.newsDetail!!.url
                    )
                    holder.newsFeedItemBinding.root.context.startActivity(intent)
                }
            }
        }
    }

    override fun getItemCount() = newsList.size

    class NewsListHolder(val newsFeedItemBinding: NewsFeedItemBinding) :
        RecyclerView.ViewHolder(newsFeedItemBinding.root)

}