package com.example.sample

import android.graphics.Bitmap
import android.view.MenuItem
import android.view.View
import com.example.sample.databinding.ActivityWebLinkOpenBinding
import im.delight.android.webview.AdvancedWebView

class WebLinkOpen : BaseActivity<ActivityWebLinkOpenBinding>(), AdvancedWebView.Listener {
    private var url: String? = null
    override fun getLayoutId() = R.layout.activity_web_link_open

    override fun initControl() {
        setSupportActionBar(binding.iToolbar.toolbar)

        binding.webView.setListener(this, this);
        binding.webView.setMixedContentAllowed(false);
        intent.getStringExtra("URL")?.let { url ->
            binding.webView.loadUrl(url);
        }
    }

    override fun onPause() {
        binding.webView.onPause()
        super.onPause()
    }

    override fun onResume() {
        binding.webView.onResume()
        super.onResume()
    }

    override fun onDestroy() {
        binding.webView.onDestroy()
        super.onDestroy()
    }

    // if you press Back button this code will work
    override fun onBackPressed() {
        // if your webview can go back it will go back
         if (!binding.webView.onBackPressed()) { return; }
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> {

                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onPageStarted(url: String?, favicon: Bitmap?) {
    }

    override fun onPageFinished(url: String?) {
        binding.webView.title?.let { title ->
            binding.iToolbar.toolbar.title = title
        }
        binding.progressBar.visibility = View.GONE
    }

    override fun onPageError(errorCode: Int, description: String?, failingUrl: String?) {
        binding.progressBar.visibility = View.GONE
    }

    override fun onDownloadRequested(
        url: String?,
        suggestedFilename: String?,
        mimeType: String?,
        contentLength: Long,
        contentDisposition: String?,
        userAgent: String?
    ) {

    }

    override fun onExternalPageRequest(url: String?) {

    }
}