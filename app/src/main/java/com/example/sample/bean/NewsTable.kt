package com.example.sample.bean

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class NewsTable(
//    @SerializedName("id") val id: Long = 0L,
    @SerializedName("title") val title: String = "",
    @SerializedName("description") val description: String = "",
    @SerializedName("content") val content: String = "",
    @SerializedName("author") val author: String = "",
    @SerializedName("urlToImage") val urlToImage: String = "",
    @SerializedName("url") val url: String = "",
    @SerializedName("publishedAt") val publishedAt: String = ""
) : Parcelable
